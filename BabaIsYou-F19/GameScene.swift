//
//  GameScene.swift
//  BabaIsYou-F19
//
//  Created by Parrot on 2019-10-17.
//  Copyright © 2019 Parrot. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene, SKPhysicsContactDelegate {
    var baba:SKSpriteNode!
    let BABA_SPEED:CGFloat = 40
    var blockCountUp:Int=0
    var blockCountDown:Int=0
    var node1:SKSpriteNode!
    var node2:SKSpriteNode!
    var node3:SKSpriteNode!
    var node4:SKSpriteNode!
    var node5:SKSpriteNode!
    var node6:SKSpriteNode!
    
    var wallCollWin:Bool=false
    var FlagCollWin:Bool!=false
    var FirstExc:Bool!=true
    
    
    var wall = SKSpriteNode()
    var flag = SKSpriteNode()
    var isBlockUp = SKSpriteNode()
    var isBlockDown = SKSpriteNode()
    var flagBlock = SKSpriteNode()
    var wallBlock = SKSpriteNode()
    var stopBlock = SKSpriteNode()
    var winBlock = SKSpriteNode()
    var flagItem = SKSpriteNode()
    override func didMove(to view: SKView) {
        self.physicsWorld.contactDelegate = self
        
        // initialze the variables
        self.baba = self.childNode(withName: "baba") as! SKSpriteNode

        self.wall = self.childNode(withName: "wall") as! SKSpriteNode
        self.flag = self.childNode(withName: "flag") as! SKSpriteNode
        self.isBlockUp = self.childNode(withName: "isblockUp") as! SKSpriteNode
        self.isBlockDown = self.childNode(withName: "isblockDown") as! SKSpriteNode
         self.wallBlock = self.childNode(withName: "wallblock") as! SKSpriteNode
         self.stopBlock = self.childNode(withName: "stopblock") as! SKSpriteNode
         self.flagBlock = self.childNode(withName: "flagblock") as! SKSpriteNode
         self.winBlock = self.childNode(withName: "winblock") as! SKSpriteNode
         self.flagItem = self.childNode(withName: "flag") as! SKSpriteNode
        
         //add collision masks to wall
         //-----------------------------
         //set cateogry to the wall
        //self.wall.physicsBody!.categoryBitMask = PhysicsCategory.Wall
        // turn off collisions --> should not collide with any objects
       // self.wall.physicsBody!.collisionBitMask = PhysicsCategory.None
        
        
        
    }
   
    func didBegin(_ contact: SKPhysicsContact) {
        
        print("Something collided!")
        
        
        let nodeA = contact.bodyA.node
        let nodeB = contact.bodyB.node
        
        print("Sprite 1: \(nodeA!.name)")
        print("Sprite 2: \(nodeB!.name)")
        
        
        if (nodeA == nil || nodeB == nil) {
            return
        }
        
        
        
        
        
        if ((nodeA!.name == "stopblock"||nodeA?.name=="winblock"||nodeA!.name == "flagblock"||nodeA?.name=="wallblock") && nodeB!.name == "isblockUp") {
          //Wall dynamic set to true
            
            print("inside collided!111")
            if(blockCountUp==0){
                blockCountUp=blockCountUp+1;
                node1=nodeA as! SKSpriteNode
            }else if(blockCountUp==1){
                node2=nodeA as! SKSpriteNode
                node3=nodeB as! SKSpriteNode
                checkRule(node1In: node1, node2In: node2,node3In: node3)
            }
            
            
        }
        
        if ((nodeA!.name == "stopblock"||nodeA?.name=="winblock"||nodeA!.name == "flagblock"||nodeA?.name=="wallblock") && nodeB!.name == "isblockDown") {
            //Wall dynamic set to true
            print("inside collided!111")
            if(blockCountDown==0){
                blockCountDown=blockCountDown+1;
                node4=nodeA as! SKSpriteNode
            }else if(blockCountDown==1){
                node5=nodeA as! SKSpriteNode
                node6=nodeB as! SKSpriteNode
                checkRule(node1In: node4, node2In: node5,node3In:node6)
            }
            
            
        }
        if(FirstExc){
        if((nodeA!.name == "wallblock"||nodeA!.name == "stopblock") && nodeB!.name == "baba"){
            
            if(nodeA!.name == "wallblock"){
                nodeA!.name = "stopblock"
                blockCountDown=1
            }else if(nodeA!.name == "stopblock"){
                nodeA!.name = "wallblock"
                blockCountDown=1
            }
            
            print("wall collision removed")
            
             //self.wall.physicsBody!.collisionBitMask = PhysicsCategory.None
             self.wall.physicsBody!.categoryBitMask = PhysicsCategory.None
            
            print("last removed")
            FirstExc=false;
            
            
        }
        }
        
        
        //wall and baba collision win condition
        
        if(wallCollWin){
            if (nodeA!.name == "wall" && nodeB!.name == "baba") {
                
                print("wall and baba collision win")
                gameWin()
                
                
                
            }
        }
        
        //flag and baba collision win condition
        
        if(FlagCollWin){
            if (nodeA!.name == "flag" && nodeB!.name == "baba") {
                
                print("flag and baba collision win")
                gameWin()
                
                
                
            }
        }
        
        
        
        
//        if ((nodeA!.name == "flagblock"||nodeA?.name=="wallblock") && nodeB!.name == "isblock") {
//            //Wall dynamic set to true
//            print("inside collided!111")
//
//        }
        
        
        
        
       
        
//        if (nodeA!.name == "player" && nodeB!.name == "win") {
//            print("Congratulations")
//            // show new LEVEL
//        }
        
        print("COLLISION DETECTED")
        print("Sprite 1: \(nodeA!.name)")
        print("Sprite 2: \(nodeB!.name)")
        print("------")
    }
    
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        // GET THE POSITION WHERE THE MOUSE WAS CLICKED
        // ---------------------------------------------
        let mouseTouch = touches.first
        if (mouseTouch == nil) {
            return
        }
        let location = mouseTouch!.location(in: self)
        
        // WHAT NODE DID THE PLAYER TOUCH
        // ----------------------------------------------
        let nodeTouched = atPoint(location).name
        //print("Player touched: \(nodeTouched)")
        
        
        // GAME LOGIC: Move player based on touch
        if (nodeTouched == "upButton") {
            // move up
            self.baba.position.y = self.baba.position.y + BABA_SPEED
        }
        else if (nodeTouched == "downButton") {
            // move down
            self.baba.position.y = self.baba.position.y - BABA_SPEED
        }
        else if (nodeTouched == "leftButton") {
            // move left
            self.baba.position.x = self.baba.position.x - BABA_SPEED
        }
        else if (nodeTouched == "rightButton") {
            // move right
            self.baba.position.x = self.baba.position.x + BABA_SPEED
        }
    }
    
    
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
    }
    
    
    
    
    //Functions for active and inactive rules
    
    
    func wallInActiveRule(nodeName:String){
         print("inside function "+nodeName)
       // self.wall.physicsBody!.isDynamic = true
        
    }
   
    func wallActiveRule(){
        self.wall.physicsBody!.isDynamic = false
    }
    func flagInActiveRule(){
        self.flag.physicsBody!.isDynamic = true
    }
    func flagActiveRule(){
        self.flag.physicsBody!.isDynamic = false
    }
    
    
    
    
    
    //check the rules in the game
    
     func checkRule(node1In:SKSpriteNode,node2In:SKSpriteNode,node3In:SKSpriteNode){
        print("inside function node1 "+(node1In.name!))
        print("inside function node2 "+node2In.name!)
         print("inside function node3 "+node3In.name!)
        if((node1In.name=="wallblock" && node2In.name=="winblock")||(node2In.name=="wallblock" && node1In.name=="winblock")){
            
            //wall collition win rule activated
            wallCollWin=true
            
        }
        
        if((node1In.name=="wallblock" && node2In.name=="stopblock")||(node2In.name=="wallblock" && node1In.name=="stopblock")){
            
            print("inside wall")
            
            //wall collision win rule deactivated
            
            
             self.wall.physicsBody!.categoryBitMask = PhysicsCategory.Baba
            
            
        }
        
        if((node1In.name=="flagblock" && node2In.name=="winblock")||(node2In.name=="flagblock" && node1In.name=="winblock")){
            
            print("inside down")
            
            //flag collition win rule activated
            
            FlagCollWin=true
            
            
        }
        
        if((node1In.name=="flagblock" && node2In.name=="stopblock")||(node2In.name=="flagblock" && node1In.name=="stopblock")){
            
            //flag collision win rule deactivated
            
            
            self.flag.physicsBody!.categoryBitMask = PhysicsCategory.Baba
            
        }
        
        
        
        
        
    }
    
    
    
    
    
    //Game win call function
    func gameWin(){
    print("Congratulations")
        
    }
    
    
    
    struct PhysicsCategory {
        static let None:  UInt32 = 0
        static let IsBlock:   UInt32 = 0b1      // 0x00000001 = 1
        static let Wall: UInt32 = 0b10     // 0x00000010 = 2
        static let Baba:   UInt32 = 0b100    // 0x00000100 = 4
        static let Blocks: UInt32 = 0b1000   // 0x00001000 = 8
        
        
    }
}
